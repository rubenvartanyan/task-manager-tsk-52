package ru.vartanyan.tm;

import ru.vartanyan.tm.bootstrap.Bootstrap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="yourRootElementName")
@XmlAccessorType(XmlAccessType.FIELD)

public class Application {

    public static void main(String[] args) throws Throwable {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
