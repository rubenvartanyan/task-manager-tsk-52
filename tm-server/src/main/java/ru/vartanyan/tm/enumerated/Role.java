package ru.vartanyan.tm.enumerated;

public enum Role {

    USER("user"),
    ADMIN("administrator");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
